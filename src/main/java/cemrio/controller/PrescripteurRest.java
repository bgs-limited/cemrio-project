
package cemrio.controller;

import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cemrio.dto.prescripteur.PrescripteurDtoReq;
import cemrio.entity.Prescripteur;
import cemrio.service.IPrescripteurService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("/prescripteurs")
@Tag(name = "prescripteur", description = "A propos du prescripteur")
public class PrescripteurRest {

	@Autowired
	private IPrescripteurService prescripteurService;

	@Autowired
	private ModelMapper modelMapper;

	@Operation(summary = "Ajoute un prescripteur dans la BD", tags = "prescripteur", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = Prescripteur.class)))),
			@ApiResponse(responseCode = "400", description = "Erreur: Ce prescripteur existe déjà", content = @Content(mediaType = "Application/Json")),
			@ApiResponse(responseCode = "403", description = "Forbidden : accès refusé", content = @Content(mediaType = "Application/Json")),
			@ApiResponse(responseCode = "401", description = "Full authentication is required to access this resource", content = @Content(mediaType = "Application/Json")) })
	@PostMapping
	@PreAuthorize("hasRole('PRIVE1') or hasRole('PRIVE2')")
	public ResponseEntity<?> add(@RequestBody PrescripteurDtoReq prDtoReq) {
		Prescripteur p = modelMapper.map(prDtoReq, Prescripteur.class);
		return prescripteurService.add(p);
	}

	@Operation(summary = "Liste paginée et par ordre croissant de nom de tous les prescripteurs crées", tags = "prescripteur", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = Prescripteur.class)))),
			@ApiResponse(responseCode = "403", description = "Forbidden : accès refusé", content = @Content(mediaType = "Application/Json")),
			@ApiResponse(responseCode = "401", description = "Full authentication is required to access this resource", content = @Content(mediaType = "Application/Json")) })
	@GetMapping
	public ResponseEntity<Page<Prescripteur>> getAll() {
		Page<Prescripteur> prescripteurs = prescripteurService.getAll();
		return ResponseEntity.ok(prescripteurs);
	}

	@Operation(summary = "Supprime un prescripteur de la BD", tags = "prescripteur", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération"),
			@ApiResponse(responseCode = "403", description = "Forbidden : accès refusé", content = @Content(mediaType = "Application/Json")),
			@ApiResponse(responseCode = "401", description = "Full authentication is required to access this resource", content = @Content(mediaType = "Application/Json")) })
	@DeleteMapping("/{id:[0-9]+}")
	@PreAuthorize("hasRole('PRIVE1') or hasRole('PRIVE2')")
	public ResponseEntity<String> delete(@PathVariable Long id) {
		prescripteurService.delete(id);
		return ResponseEntity.ok("Prescripteur supprimé avec succès");
	}

	@Operation(summary = "Recupère un prescripteur dans la BD", tags = "prescripteur", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = Prescripteur.class)))),
			@ApiResponse(responseCode = "404", description = "Le Prescripteur n'a pas été trouvé dans la BD", content = @Content(mediaType = "Application/Json")),
			@ApiResponse(responseCode = "403", description = "Forbidden : accès refusé", content = @Content(mediaType = "Application/Json")),
			@ApiResponse(responseCode = "401", description = "Full authentication is required to access this resource", content = @Content(mediaType = "Application/Json")) })
	@GetMapping("/{id:[0-9]+}")
	public ResponseEntity<Prescripteur> get(@PathVariable Long id) {
		Prescripteur prescripteur = prescripteurService.get(id);
		return ResponseEntity.ok(prescripteur);
	}

	@Operation(summary = "Recherche un prescripteur dans la BD à partir de son nom", tags = "prescripteur", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = Prescripteur.class)))),
			@ApiResponse(responseCode = "404", description = "Le Patient non trouvé dans la BD", content = @Content(mediaType = "Application/Json")),
			@ApiResponse(responseCode = "403", description = "Forbidden : accès refusé", content = @Content(mediaType = "Application/Json")),
			@ApiResponse(responseCode = "401", description = "Full authentication is required to access this resource", content = @Content(mediaType = "Application/Json")) })
	@GetMapping("/search/{nom}")
	public ResponseEntity<List<Prescripteur>> search(@PathVariable @Valid String nom) {
		List<Prescripteur> prescripteurs = prescripteurService.getByNom(nom);
		return ResponseEntity.ok(prescripteurs);
	}

	@Operation(summary = "modifie le nom d'un prescripteur ", tags = "prescripteur", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = Prescripteur.class)))),
			@ApiResponse(responseCode = "403", description = "Forbidden : accès refusé", content = @Content(mediaType = "Application/Json")),
			@ApiResponse(responseCode = "401", description = "Full authentication is required to access this resource", content = @Content(mediaType = "Application/Json")) })
	@PutMapping("/{id:[0-9]+}/nom")
	@PreAuthorize("hasRole('PRIVE1') or hasRole('PRIVE2')")
	public ResponseEntity<Prescripteur> editName(@PathVariable Long id, @Valid @RequestParam String nom) {
		Prescripteur prescripteur = prescripteurService.editName(id, nom);
		return ResponseEntity.ok(prescripteur);
	}

	@Operation(summary = "modifie le prénom d'un prescripteur ", tags = "prescripteur", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = Prescripteur.class)))),
			@ApiResponse(responseCode = "403", description = "Forbidden : accès refusé", content = @Content(mediaType = "Application/Json")),
			@ApiResponse(responseCode = "401", description = "Full authentication is required to access this resource", content = @Content(mediaType = "Application/Json")) })
	@PutMapping("/{id:[0-9]+}/prenom")
	@PreAuthorize("hasRole('PRIVE1') or hasRole('PRIVE2')")
	public ResponseEntity<Prescripteur> editSurname(@PathVariable Long id, @Valid @RequestParam String prenom) {
		Prescripteur prescripteur = prescripteurService.editSurname(id, prenom);
		return ResponseEntity.ok(prescripteur);
	}

	@Operation(summary = "modifie le telephone d'un prescripteur ", tags = "prescripteur", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = Prescripteur.class)))),
			@ApiResponse(responseCode = "403", description = "Forbidden : accès refusé", content = @Content(mediaType = "Application/Json")),
			@ApiResponse(responseCode = "401", description = "Full authentication is required to access this resource", content = @Content(mediaType = "Application/Json")) })
	@PutMapping("/{id:[0-9]+}/telephone")
	@PreAuthorize("hasRole('PRIVE1') or hasRole('PRIVE2')")
	public ResponseEntity<Prescripteur> editTelephone(@PathVariable Long id, @Valid @RequestParam String telephone) {
		Prescripteur prescripteur = prescripteurService.editTelephone(id, telephone);
		return ResponseEntity.ok(prescripteur);
	}

	@Operation(summary = "modifie l'email d'un prescripteur ", tags = "prescripteur", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = Prescripteur.class)))),
			@ApiResponse(responseCode = "404", description = "Erreur: User non trouvé dans la BD", content = @Content(mediaType = "Application/Json")),
			@ApiResponse(responseCode = "403", description = "Forbidden : accès refusé", content = @Content(mediaType = "Application/Json")),
			@ApiResponse(responseCode = "401", description = "Full authentication is required to access this resource", content = @Content(mediaType = "Application/Json")) })
	@PutMapping("/{id:[0-9]+}/email")
	@PreAuthorize("hasRole('PRIVE1') or hasRole('PRIVE2')")
	public ResponseEntity<Prescripteur> editEmail(@PathVariable Long id, @Valid @RequestParam String email) {
		Prescripteur prescripteur = prescripteurService.editEmail(id, email);
		return ResponseEntity.ok(prescripteur);
	}

	@Operation(summary = "Modifie un prescripteur dans la BD", tags = "prescripteur", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = Prescripteur.class)))),
			@ApiResponse(responseCode = "403", description = "Forbidden : accès refusé", content = @Content(mediaType = "Application/Json")),
			@ApiResponse(responseCode = "401", description = "Full authentication is required to access this resource", content = @Content(mediaType = "Application/Json")) })
	@PutMapping("/{id:[0-9]+}")
	@PreAuthorize("hasRole('PRIVE1') or hasRole('PRIVE2')")
	public ResponseEntity<Prescripteur> edit(@PathVariable Long id, @Valid @RequestBody PrescripteurDtoReq prDtoreq) {
		Prescripteur p = modelMapper.map(prDtoreq,
				Prescripteur.class);
		Prescripteur prescripteur = prescripteurService.edit(id, p);
		return ResponseEntity.ok(prescripteur);
	}
}
