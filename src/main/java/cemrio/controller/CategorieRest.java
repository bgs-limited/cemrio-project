package cemrio.controller;

import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cemrio.dto.categorie.CategorieDtoReq;
import cemrio.entity.Categorie;
import cemrio.service.IcategorieService;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;

@OpenAPIDefinition(info = @Info(title = "API CEMRIO", description = "Documentation de l'API", version = "1.0"))
@RestController
@RequestMapping("/categories")
@Tag(name = "categorie", description = "A propos des categories d'examen")
public class CategorieRest {

	@Autowired
	private IcategorieService categorieService;

	@Autowired
	private ModelMapper modelMapper;

	@Operation(summary = "Ajoute une catégorie dans la BD", tags = "categorie", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = Categorie.class)))),
			@ApiResponse(responseCode = "400", description = "Erreur au niveau dela requete", content = @Content(mediaType = "Application/Json")),
			@ApiResponse(responseCode = "403", description = "Forbidden : accès refusé", content = @Content(mediaType = "Application/Json")),
			@ApiResponse(responseCode = "401", description = "Full authentication is required to access this resource", content = @Content(mediaType = "Application/Json")) })
	@PostMapping
	@PreAuthorize("hasRole('PRIVE1') or hasRole('PRIVE2')")
	public ResponseEntity<Categorie> add(@RequestBody CategorieDtoReq catDtoReq) {
		Categorie cat = modelMapper.map(catDtoReq, Categorie.class);
		Categorie categorie = categorieService.add(cat);
		return ResponseEntity.ok(categorie);
	}

	@Operation(summary = "Recupère une catégorie dans la BD à partir de son id", tags = "categorie", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = Categorie.class)))),
			@ApiResponse(responseCode = "404", description = "Catégorie non trouvé dans la BD", content = @Content(mediaType = "Application/Json")),
			@ApiResponse(responseCode = "403", description = "Forbidden : aacès refusé", content = @Content(mediaType = "Application/Json")),
			@ApiResponse(responseCode = "401", description = "Full authentication is required to access this resource", content = @Content(mediaType = "Application/Json")) })
	@GetMapping("/{id:[0-9]+}")
	public ResponseEntity<Categorie> get(@PathVariable Long id) {
		Categorie categorie = categorieService.get(id);
		return ResponseEntity.ok(categorie);
	}

	@Operation(summary = "Liste paginée et triée par ordre croissant de nom de toutes les catégories crées", tags = "categorie", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = Categorie.class)))),
			@ApiResponse(responseCode = "403", description = "Forbidden : aacès refusé", content = @Content(mediaType = "Application/Json")),
			@ApiResponse(responseCode = "401", description = "Full authentication is required to access this resource", content = @Content(mediaType = "Application/Json")) })
	@GetMapping
	public ResponseEntity<Page<Categorie>> getAll() {
		Page<Categorie> categories = categorieService.getAll();
		return ResponseEntity.ok(categories);
	}

	@Operation(summary = "Recherche une catégorie dans la BD à partir de son nom", tags = "categorie", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = Categorie.class)))),
			@ApiResponse(responseCode = "404", description = "Catégorie non trouvé dans la BD", content = @Content(mediaType = "Application/Json")),
			@ApiResponse(responseCode = "403", description = "Forbidden : aacès refusé", content = @Content(mediaType = "Application/Json")),
			@ApiResponse(responseCode = "401", description = "Full authentication is required to access this resource", content = @Content(mediaType = "Application/Json")) })
	@GetMapping("/search/{nom}")
	public ResponseEntity<List<Categorie>> search(@PathVariable @Valid String nom) {
		List<Categorie> categories = categorieService.getByNom(nom);
		return ResponseEntity.ok(categories);
	}

	@Operation(summary = "Supprime une catégorie de la BD", tags = "catégorie", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération"),
			@ApiResponse(responseCode = "403", description = "Forbidden : aacès refusé", content = @Content(mediaType = "Application/Json")),
			@ApiResponse(responseCode = "401", description = "Full authentication is required to access this resource", content = @Content(mediaType = "Application/Json")) })
	@DeleteMapping("/{id:[0-9]+}")
	@PreAuthorize("hasRole('PRIVE1') or hasRole('PRIVE2')")
	public ResponseEntity<String> delete(@PathVariable Long id) {
		categorieService.delete(id);
		return ResponseEntity.ok("Catégorie supprimé avec succès");
	}

	@Operation(summary = "modifie le nom d'une catégorie ", tags = "catégorie", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = Categorie.class)))),
			@ApiResponse(responseCode = "403", description = "Forbidden : aacès refusé", content = @Content(mediaType = "Application/Json")),
			@ApiResponse(responseCode = "401", description = "Full authentication is required to access this resource", content = @Content(mediaType = "Application/Json")) })
	@PutMapping("/{id:[0-9]+}/nom")
	@PreAuthorize("hasRole('PRIVE1') or hasRole('PRIVE2')")
	public ResponseEntity<Categorie> editName(@PathVariable Long id, @Valid @RequestParam String nom) {
		Categorie categorie = categorieService.editName(id, nom);
		return ResponseEntity.ok(categorie);
	}

	@Operation(summary = "modifie la catégorie parente d'une catégorie ", tags = "catégorie", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = Categorie.class)))),
			@ApiResponse(responseCode = "403", description = "Forbidden : aacès refusé", content = @Content(mediaType = "Application/Json")),
			@ApiResponse(responseCode = "401", description = "Full authentication is required to access this resource", content = @Content(mediaType = "Application/Json")) })
	@PutMapping("/{id:[0-9]+}/categorie-parent")
	@PreAuthorize("hasRole('PRIVE1') or hasRole('PRIVE2')")
	public ResponseEntity<Categorie> editCategorieParent(@PathVariable Long id, @Valid @RequestParam Long catParentId) {
		Categorie categorie = categorieService.editCategorieParent(id, catParentId);
		return ResponseEntity.ok(categorie);
	}

	@Operation(summary = "modifie le service auquel appartient une catégorie ", tags = "catégorie", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = Categorie.class)))),
			@ApiResponse(responseCode = "403", description = "Forbidden : aacès refusé", content = @Content(mediaType = "Application/Json")),
			@ApiResponse(responseCode = "401", description = "Full authentication is required to access this resource", content = @Content(mediaType = "Application/Json")) })
	@PutMapping("/{id:[0-9]+}/service")
	@PreAuthorize("hasRole('PRIVE1') or hasRole('PRIVE2')")
	public ResponseEntity<Categorie> editService(@PathVariable Long id, @Valid @RequestParam Long serviceId) {
		Categorie categorie = categorieService.editService(id, serviceId);
		return ResponseEntity.ok(categorie);
	}

	@Operation(summary = "modifie la description d'une catégorie ", tags = "catégorie", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = Categorie.class)))),
			@ApiResponse(responseCode = "403", description = "Forbidden : aacès refusé", content = @Content(mediaType = "Application/Json")),
			@ApiResponse(responseCode = "401", description = "Full authentication is required to access this resource", content = @Content(mediaType = "Application/Json")) })
	@PutMapping("/{id:[0-9]+}/description")
	@PreAuthorize("hasRole('PRIVE1') or hasRole('PRIVE2')")
	public ResponseEntity<Categorie> editDescription(@PathVariable Long id, @Valid @RequestParam String description) {
		Categorie categorie = categorieService.editDescription(id, description);
		return ResponseEntity.ok(categorie);
	}

	@Operation(summary = "modifie une catégorie de la BD", tags = "catégorie", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = Categorie.class)))),
			@ApiResponse(responseCode = "403", description = "Forbidden : aacès refusé", content = @Content(mediaType = "Application/Json")),
			@ApiResponse(responseCode = "401", description = "Full authentication is required to access this resource", content = @Content(mediaType = "Application/Json")) })
	@PutMapping("/{id:[0-9]+}")
	@PreAuthorize("hasRole('PRIVE1') or hasRole('PRIVE2')")
	public ResponseEntity<Categorie> edit(@PathVariable Long id, @RequestBody @Valid CategorieDtoReq catDtoReq) {
		Categorie categorie = modelMapper.map(catDtoReq, Categorie.class);
		Categorie cat = categorieService.edit(id, categorie);
		return ResponseEntity.ok(cat);
	}

}
