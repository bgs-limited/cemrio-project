
package cemrio.controller;

import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cemrio.dto.examen.ExamenDtoReq;
import cemrio.entity.Examen;
import cemrio.service.IExamenService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("/examens")
@Tag(name = "examen", description = "A propos des examens")
public class ExamenRest {

	@Autowired
	private IExamenService examenService;

	@Autowired
	private ModelMapper modelMapper;

	@Operation(summary = "Ajoute un examen dans la BD", tags = "examen", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = Examen.class)))),
			@ApiResponse(responseCode = "400", description = "Erreur: Ce nom d'utilisateur est déjà utilisé/Erreur: Cet email est déjà utilisé", content = @Content(mediaType = "Application/Json")),
			@ApiResponse(responseCode = "403", description = "Forbidden : accès refusé", content = @Content(mediaType = "Application/Json")),
			@ApiResponse(responseCode = "401", description = "Full authentication is required to access this resource", content = @Content(mediaType = "Application/Json")) })
	@PostMapping
	@PreAuthorize("hasRole('PRIVE1') or hasRole('PRIVE2')")
	public ResponseEntity<Examen> add(@RequestBody ExamenDtoReq examDtoReq) {
		Examen e = modelMapper.map(examDtoReq, Examen.class);
		Examen examen = examenService.add(e);
		return ResponseEntity.ok(examen);
	}

	@Operation(summary = "Liste paginée et par ordre croissant de nom de tous les examens disponibles", tags = "examen", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = Examen.class)))),
			@ApiResponse(responseCode = "400", description = "Erreur: Ce nom d'utilisateur est déjà utilisé/Erreur: Cet email est déjà utilisé", content = @Content(mediaType = "Application/Json")),
			@ApiResponse(responseCode = "403", description = "Forbidden : accès refusé", content = @Content(mediaType = "Application/Json")),
			@ApiResponse(responseCode = "401", description = "Full authentication is required to access this resource", content = @Content(mediaType = "Application/Json")) })
	@GetMapping
	public ResponseEntity<Page<Examen>> getAll() {
		Page<Examen> examens = examenService.getAll();
		return ResponseEntity.ok(examens);
	}

	@Operation(summary = "Recupère un examen dans la BD à partir de son id", tags = "examen", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = Examen.class)))),
			@ApiResponse(responseCode = "404", description = "Examen non trouvé dans la BD", content = @Content(mediaType = "Application/Json")),
			@ApiResponse(responseCode = "400", description = "Erreur: Ce nom d'utilisateur est déjà utilisé/Erreur: Cet email est déjà utilisé", content = @Content(mediaType = "Application/Json")),
			@ApiResponse(responseCode = "403", description = "Forbidden : accès refusé", content = @Content(mediaType = "Application/Json")),
			@ApiResponse(responseCode = "401", description = "Full authentication is required to access this resource", content = @Content(mediaType = "Application/Json")) })
	@GetMapping("/{id:[0-9]+}")
	public ResponseEntity<Examen> get(@PathVariable Long id) {
		Examen examen = examenService.get(id);
		return ResponseEntity.ok(examen);
	}

	@Operation(summary = "Recherche un examen dans la BD à partir de son nom", tags = "examen", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = Examen.class)))),
			@ApiResponse(responseCode = "404", description = "Examen non trouvé dans la BD", content = @Content(mediaType = "Application/Json")),
			@ApiResponse(responseCode = "400", description = "Erreur: Ce nom d'utilisateur est déjà utilisé/Erreur: Cet email est déjà utilisé", content = @Content(mediaType = "Application/Json")),
			@ApiResponse(responseCode = "403", description = "Forbidden : accès refusé", content = @Content(mediaType = "Application/Json")),
			@ApiResponse(responseCode = "401", description = "Full authentication is required to access this resource", content = @Content(mediaType = "Application/Json")) })
	@GetMapping("/search/{nom}")
	public ResponseEntity<List<Examen>> search(@PathVariable @Valid String nom) {
		List<Examen> examens = examenService.getByNom(nom);
		return ResponseEntity.ok(examens);
	}

	@Operation(summary = "Supprime un examen de la BD", tags = "examen", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération"),
			@ApiResponse(responseCode = "400", description = "Erreur: Ce nom d'utilisateur est déjà utilisé/Erreur: Cet email est déjà utilisé", content = @Content(mediaType = "Application/Json")),
			@ApiResponse(responseCode = "403", description = "Forbidden : accès refusé", content = @Content(mediaType = "Application/Json")),
			@ApiResponse(responseCode = "401", description = "Full authentication is required to access this resource", content = @Content(mediaType = "Application/Json")) })
	@DeleteMapping("/{id:[0-9]+}")
	@PreAuthorize("hasRole('PRIVE1') or hasRole('PRIVE2')")
	public ResponseEntity<String> delete(@PathVariable Long id) {
		examenService.delete(id);
		return ResponseEntity.ok("examen supprimé avec succès");
	}

	@Operation(summary = "modifie le nom d'un examen ", tags = "examen", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = Examen.class)))),
			@ApiResponse(responseCode = "400", description = "Erreur: Ce nom d'utilisateur est déjà utilisé/Erreur: Cet email est déjà utilisé", content = @Content(mediaType = "Application/Json")),
			@ApiResponse(responseCode = "403", description = "Forbidden : accès refusé", content = @Content(mediaType = "Application/Json")),
			@ApiResponse(responseCode = "401", description = "Full authentication is required to access this resource", content = @Content(mediaType = "Application/Json")) })
	@PutMapping("/{id:[0-9]+}/nom")
	@PreAuthorize("hasRole('PRIVE1') or hasRole('PRIVE2')")
	public ResponseEntity<Examen> editName(@PathVariable Long id, @Valid @RequestParam String nom) {
		Examen examen = examenService.editName(id, nom);
		return ResponseEntity.ok(examen);
	}

	@Operation(summary = "modifie la description d'un examen ", tags = "examen", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = Examen.class)))),
			@ApiResponse(responseCode = "400", description = "Erreur: Ce nom d'utilisateur est déjà utilisé/Erreur: Cet email est déjà utilisé", content = @Content(mediaType = "Application/Json")),
			@ApiResponse(responseCode = "403", description = "Forbidden : accès refusé", content = @Content(mediaType = "Application/Json")),
			@ApiResponse(responseCode = "401", description = "Full authentication is required to access this resource", content = @Content(mediaType = "Application/Json")) })
	@PutMapping("/{id:[0-9]+}/description")
	@PreAuthorize("hasRole('PRIVE1') or hasRole('PRIVE2')")
	public ResponseEntity<Examen> editDescription(@PathVariable Long id, @Valid @RequestParam String description) {
		Examen examen = examenService.editDescription(id, description);
		return ResponseEntity.ok(examen);
	}

	@Operation(summary = "modifie la catégorie d'un examen ", tags = "examen", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = Examen.class)))),
			@ApiResponse(responseCode = "400", description = "Erreur: Ce nom d'utilisateur est déjà utilisé/Erreur: Cet email est déjà utilisé", content = @Content(mediaType = "Application/Json")),
			@ApiResponse(responseCode = "403", description = "Forbidden : accès refusé", content = @Content(mediaType = "Application/Json")),
			@ApiResponse(responseCode = "401", description = "Full authentication is required to access this resource", content = @Content(mediaType = "Application/Json")) })
	@PutMapping("/{id:[0-9]+}/categorie")
	@PreAuthorize("hasRole('PRIVE1') or hasRole('PRIVE2')")
	public ResponseEntity<Examen> editCategory(@PathVariable Long id, @Valid @RequestParam Long categorieId) {
		Examen examen = examenService.editCategory(id, categorieId);
		return ResponseEntity.ok(examen);
	}

	@Operation(summary = "modifie un examen ", tags = "examen", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = Examen.class)))),
			@ApiResponse(responseCode = "400", description = "Erreur: Ce nom d'utilisateur est déjà utilisé/Erreur: Cet email est déjà utilisé", content = @Content(mediaType = "Application/Json")),
			@ApiResponse(responseCode = "403", description = "Forbidden : accès refusé", content = @Content(mediaType = "Application/Json")),
			@ApiResponse(responseCode = "401", description = "Full authentication is required to access this resource", content = @Content(mediaType = "Application/Json")) })
	@PutMapping("/{id:[0-9]+}")
	@PreAuthorize("hasRole('PRIVE1') or hasRole('PRIVE2')")
	public ResponseEntity<Examen> edit(@PathVariable Long id, @Valid @RequestBody Examen exam) {
		Examen examen = examenService.edit(id, exam);
		return ResponseEntity.ok(examen);
	}

}
