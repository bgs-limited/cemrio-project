
package cemrio.dto.patient;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PatientDtoReq {

	@Schema(description = "nom du patient", example = "Bouchua")
	@NotNull
	@Size(min = 1, max = 20)
	private String nom;

	@Schema(description = "prénom du patient", example = "Stéphane")
	@Size(min = 1, max = 100)
	private String prenom;

	@Schema(description = "age du patient", example = "17")
	@Column(nullable = true)
	private float age;

	@NotNull
	@Schema(description = "sexe du patient", example = "M")
	private char sexe;

	@Schema(description = "profession du patient", example = "Ingénieur Informatique", minLength = 1, maxLength = 80)
	@Size(min = 1, max = 80)
	private String profession;

	@Schema(description = "Téléphone du patient", example = "696225977")
	@Pattern(regexp = "^6(9|8|7|6|5)[0-9]{7}$")
	private String telephone;

	@Schema(description = "renseignements Cliniques du patient", example = "raisons de l'examen")
	@NotNull
	private String renseignementsCliniques;

	@Schema(description = "date des dernières règles du patient", example = "25-08-2020")
	@JsonFormat(shape = Shape.STRING, pattern = "dd-MM-yyyy")
	private LocalDate ddr;

	// private Set<PatientExamPresc> patientExamPrescs;

	// private User user;
}
