
package cemrio.dto.user;

import java.util.Set;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import cemrio.entity.Role;
import cemrio.entity.Service;
import lombok.Getter;
import lombok.Setter;

@Getter

@Setter
public class UserAddDto {

	@NotNull
	@Size(min = 3, max = 20)
	private String username;

	@Size(max = 50)
	@Email
	private String email;

	@NotNull
	@Size(min = 6, max = 40)
	@JsonIgnore
	private String password = "000000";

	private Set<Role> roles;

	private Set<Service> services;

}
