
package cemrio.dto.user;

import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import cemrio.entity.Role;
import lombok.Getter;
import lombok.Setter;

@Getter

@Setter
public class UserEditByAdminDto {

	@NotNull
	@Size(min = 3, max = 20)
	private String username;

	@Email
	@Size(max = 50)
	private String email;

	private Set<Role> roles = new HashSet<>();
}
