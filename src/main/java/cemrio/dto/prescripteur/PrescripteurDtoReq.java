
package cemrio.dto.prescripteur;

import javax.persistence.Column;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PrescripteurDtoReq {

	@NotNull
	@Size(min = 1, max = 100)
	private String nom;

	@NotNull
	@Size(min = 1, max = 100)
	private String prenom;

	@Pattern(regexp = "^6(9|8|7|6|5)[0-9]{7}$")
	private String telephone;

	@Column(unique = true)
	@Email
	@Size(max = 50)
	private String email;
}
