package cemrio.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import cemrio.entity.Service;

public interface IserviceRepo extends JpaRepository<Service, Long> {

}
