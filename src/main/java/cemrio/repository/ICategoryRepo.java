package cemrio.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import cemrio.entity.Categorie;

public interface ICategoryRepo extends JpaRepository<Categorie, Long> {

	List<Categorie> findByNomIgnoreCase(String nom);

}
