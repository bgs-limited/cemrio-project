package cemrio.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import cemrio.entity.Privilege;
import cemrio.entity.Role;

public interface IPrivilegeRepo extends JpaRepository<Privilege, Long> {
	Privilege findTopByActionAndEntityAndRolesInOrderByRolesDesc(String action, String entity, Set<Role> roles);
}
