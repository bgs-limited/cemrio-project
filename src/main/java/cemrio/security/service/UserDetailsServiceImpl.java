
package cemrio.security.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cemrio.entity.User;
import cemrio.repository.IUserRepo;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private IUserRepo userRepo;

	@Override
	@Transactional
	public UserDetails loadUserByUsername(String username) {
		User user = userRepo.findByUsername(username)
				.orElseThrow(() -> new UsernameNotFoundException("l'utilisateur " + username + " n'a pas été trouvé"));
		return UserDetailsImpl.build(user);
	}

}
