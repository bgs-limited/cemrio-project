
package cemrio.service;

import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;

import cemrio.dto.user.UserEditPasswordDto;
import cemrio.entity.User;

public interface IUserService {
	public ResponseEntity<?> add(User user);

	public User get(Long id);

	public Page<User> getAll();

	public User editEmail(Long id, String email);

	public User editByAdmin(Long id, User u);

	public ResponseEntity<String> editPassword(Long id, UserEditPasswordDto userEditPasswordDto);

	public void resetPassword(Long id);

	public void delete(Long id);

	public User addService(Long id, Long serviceId);

	public ResponseEntity<?> editService(Long id, Long oldServiceId, Long newServiceId);

	public User editRole(Long id, Long roleId);

	public User editUsername(Long id, String username);
}
