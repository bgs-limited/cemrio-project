package cemrio.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cemrio.entity.Categorie;
import cemrio.entity.User;
import cemrio.exception.CategoryNotFoundException;
import cemrio.repository.ICategoryRepo;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CategorieServiceImpl implements IcategorieService {

	@Autowired
	private ICategoryRepo categorieRepo;

	@Autowired
	private IServiceService service;

	@Autowired
	private AuthorizationService authorizationService;

	@Override
	@Transactional
	public Categorie add(Categorie cat) {
		User user = authorizationService.getUser();
		cat.setUser(user);
		Categorie categorie = categorieRepo.save(cat);
		log.info("Catégorie " + cat.getNom() + " crée" + " par " + user.getUsername());
		return categorie;
	}

	@Override
	public Categorie get(Long id) {
		return categorieRepo.findById(id)
				.orElseThrow(() -> new RuntimeException("catégorie de id " + id + " non trouvée"));

	}

	@Override
	public Page<Categorie> getAll() {
		return categorieRepo.findAll(PageRequest.of(0, 30, Sort.by(Sort.Direction.ASC, "nom")));

	}

	@Override
	public List<Categorie> getByNom(@Valid String nom) {
		List<Categorie> categories = categorieRepo.findByNomIgnoreCase(nom);
		if (categories.isEmpty()) {
			throw new CategoryNotFoundException("La catégorie " + nom + " est introuvable");
		}
		return categories;
	}

	@Override
	@Transactional
	public void delete(Long id) {
		log.info("Catégorie de id " + id + " supprimé par l'utilisateur " + authorizationService.getUser());
		categorieRepo.deleteById(id);

	}

	@Override
	@Transactional
	public Categorie editName(Long id, @Valid String nom) {
		Categorie categorie = get(id);
		categorie.setNom(nom);
		User user = authorizationService.getUser();
		categorie.setUser(user);
		return categorieRepo.save(categorie);
	}

	@Override
	@Transactional
	public Categorie editDescription(Long id, @Valid String description) {
		Categorie categorie = get(id);
		categorie.setDescription(description);
		User user = authorizationService.getUser();
		categorie.setUser(user);
		return categorieRepo.save(categorie);
	}

	@Override
	@Transactional
	public Categorie editCategorieParent(Long id, @Valid Long catParentId) {
		Categorie categorie = get(id);
		categorie.setCategoryParent(get(catParentId));
		User user = authorizationService.getUser();
		categorie.setUser(user);
		return categorieRepo.save(categorie);
	}

	@Override
	@Transactional
	public Categorie editService(Long id, @Valid Long serviceId) {
		Categorie categorie = get(id);
		categorie.setService(service.get(serviceId));
		User user = authorizationService.getUser();
		categorie.setUser(user);
		return categorieRepo.save(categorie);
	}

	@Override
	@Transactional
	public Categorie edit(Long id, Categorie cat) {
		Categorie categorie = get(id);
		categorie.setNom(cat.getNom());
		categorie.setDescription(cat.getDescription());
		categorie.setCategoryParent(cat.getCategoryParent());
		categorie.setService(cat.getService());
		User user = authorizationService.getUser();
		categorie.setUser(user);
		return categorieRepo.save(categorie);
	}

}
