package cemrio.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cemrio.dto.patient.PatientDtoExportReq;
import cemrio.entity.ERole;
import cemrio.entity.Examen;
import cemrio.entity.Patient;
import cemrio.entity.PatientExamPresc;
import cemrio.entity.Prescripteur;
import cemrio.entity.Role;
import cemrio.entity.User;
import cemrio.exception.PatientNotFoundException;
import cemrio.repository.IPatientExamPrescRepo;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PatientExamPrescServiceImpl implements IPatientExamPrescService {

	@Autowired
	private AuthorizationService authorizationService;

	@Autowired
	private IPatientService patientService;

	@Autowired
	private IExamenService examenService;

	@Autowired
	private IPrescripteurService prescripteurService;

	@Autowired
	private IPatientExamPrescRepo patientExamPrescRepo;

	private static final String MESSAGEERROREXPORT = "Aucun patient ne correspond à ces critères";

	private static final LocalDateTime FROM = LocalDate.now().atTime(8, 0, 0, 0);
	private static final LocalDateTime TO = LocalDateTime.now();
	private static final PageRequest SORT = PageRequest.of(0, 30, Sort.by(Sort.Direction.ASC, "patient"));

	@Override
	@Transactional
	public Set<PatientExamPresc> add(Set<PatientExamPresc> p) {
		User user = authorizationService.getUser();
		for (PatientExamPresc u : p) {
			u.setUser(user);
			patientExamPrescRepo.save(u);
		}
		return p;
	}

	@Override
	public Set<PatientExamPresc> get(Patient p) {
		return patientExamPrescRepo.findByPatient(p);
	}

	@Override
	public PatientExamPresc get(Long id) {
		return patientExamPrescRepo.findById(id).orElseThrow(
				() -> new RuntimeException("La ligne Patient-Examen-Prescripteur d'id " + id + "n'a pas été trouvée"));
	}

	@Override
	public Page<PatientExamPresc> getAllPatientsWithExamensAndPrescripteurs() {
		User currentUser = authorizationService.getUser();
		Role userRole = authorizationService.getGreaterRoleUser(currentUser);
		switch (userRole.getNom()) {
		case ROLE_CLASSE1: {
			return patientExamPrescRepo.findAllByCreatedAtBetween(FROM.minusDays(7), TO, SORT);
		}
		case ROLE_CLASSE2: {
			return patientExamPrescRepo.findAllByCreatedAtBetween(FROM.minusDays(2), TO, SORT);
		}
		case ROLE_CLASSE3:
		case ROLE_PRIVE1: {
			return patientExamPrescRepo.findAllByCreatedAtBetween(FROM.minusMonths(2), TO, SORT);
		}
		default:
			return patientExamPrescRepo.findAll(SORT);
		}
	}

	@Override
	@Transactional
	public void delete(Long id) {
		patientExamPrescRepo.deleteById(id);
	}

	@Override
	@Transactional
	public PatientExamPresc editRenseignementsCligniques(Long id, @Valid String renseignementsCliniques) {
		PatientExamPresc patientExamPresc = get(id);
		patientExamPresc.setRenseignementsCliniques(renseignementsCliniques);
		User user = authorizationService.getUser();
		patientExamPresc.setUser(user);
		return patientExamPrescRepo.save(patientExamPresc);
	}

	@Override
	@Transactional
	public PatientExamPresc editPrescripteur(Long id, Long prescripteurId) {
		Prescripteur prescripteur = prescripteurService.get(prescripteurId);
		PatientExamPresc patientExamPresc = get(id);
		patientExamPresc.setPrescripteur(prescripteur);
		User user = authorizationService.getUser();
		patientExamPresc.setUser(user);
		return patientExamPrescRepo.save(patientExamPresc);
	}

	@Override
	@Transactional
	public PatientExamPresc editExamen(Long id, Long examenId) {
		Examen examen = examenService.get(examenId);
		PatientExamPresc patientExamPresc = get(id);
		patientExamPresc.setExamen(examen);
		User user = authorizationService.getUser();
		patientExamPresc.setUser(user);
		return patientExamPrescRepo.save(patientExamPresc);
	}

	@Override
	@Transactional
	public PatientExamPresc editPatient(Long id, Long patientId) {
		Patient patient = patientService.get(patientId);
		PatientExamPresc patientExamPresc = get(id);
		patientExamPresc.setPatient(patient);
		User user = authorizationService.getUser();
		patientExamPresc.setUser(user);
		return patientExamPrescRepo.save(patientExamPresc);
	}

	@Override
	@Transactional
	public PatientExamPresc edit(Long id, PatientExamPresc p) {
		PatientExamPresc patientExamPresc = get(id);
		patientExamPresc.setPatient(p.getPatient());
		patientExamPresc.setExamen(p.getExamen());
		patientExamPresc.setPrescripteur(p.getPrescripteur());
		patientExamPresc.setRenseignementsCliniques(p.getRenseignementsCliniques());
		User user = authorizationService.getUser();
		patientExamPresc.setUser(user);
		return patientExamPrescRepo.save(patientExamPresc);
	}

	private List<PatientExamPresc> getByDates(LocalDateTime from, LocalDateTime to) {
		if (from.isAfter(to)) {
			throw new RuntimeException("La première date doit précéder la seconde");
		}
		List<PatientExamPresc> patientsBetweenDates = patientExamPrescRepo.findAllByCreatedAtBetween(from,
				to);
		if (patientsBetweenDates.isEmpty()) {
			throw new RuntimeException(MESSAGEERROREXPORT);
		}
		return patientsBetweenDates;
	}

	private List<PatientExamPresc> getByPrescripteurs(Set<Prescripteur> p) {
		List<PatientExamPresc> patientsExamPrescByPres = patientExamPrescRepo.findByPrescripteurIn(p);
		if (patientsExamPrescByPres.isEmpty()) {
			throw new PatientNotFoundException(MESSAGEERROREXPORT);
		}
		return patientsExamPrescByPres;
	}

	private List<PatientExamPresc> getByExamens(Set<Examen> e) {
		List<PatientExamPresc> patientsExamPrescByExam = patientExamPrescRepo.findByExamenIn(e);
		if (patientsExamPrescByExam.isEmpty()) {
			throw new PatientNotFoundException(MESSAGEERROREXPORT);
		}
		return patientsExamPrescByExam;
	}

	@Override
	public List<PatientExamPresc> export(PatientDtoExportReq p) {
		List<PatientExamPresc> patientExamsPrescs = getByDates(p.getStartDate().atStartOfDay(),
				p.getEndDate().atStartOfDay());
		List<PatientExamPresc> results = new ArrayList<>();
		results.addAll(patientExamsPrescs);

		if (p.getPrescripteurs() != null) {
			List<PatientExamPresc> patientsByPres = getByPrescripteurs(p.getPrescripteurs());
			for (PatientExamPresc patient : patientExamsPrescs) {
				if (!patientsByPres.contains(patient)) {
					results.remove(patient);
				}
			}
			patientExamsPrescs.clear();
			patientExamsPrescs.addAll(results);
		}
		if (p.getExamens() != null) {
			List<PatientExamPresc> patientsByExam = getByExamens(p.getExamens());
			for (PatientExamPresc patient : patientExamsPrescs) {
				if (!patientsByExam.contains(patient)) {
					results.remove(patient);
				}
			}
			patientExamsPrescs.clear();
			patientExamsPrescs.addAll(results);
		}

		if (results.isEmpty()) {
			throw new RuntimeException(MESSAGEERROREXPORT);
		}

		User currentUser = authorizationService.getUser();
		Role userRole = authorizationService.getGreaterRoleUser(currentUser);
		if (userRole.getNom().equals(ERole.ROLE_CLASSE3)) {
			Set<cemrio.entity.Service> servicesUser = currentUser.getServices();
			for (PatientExamPresc patient : patientExamsPrescs) {
				cemrio.entity.Service service = patient.getExamen().getCategorie().getService();
				if (!servicesUser.contains(service)) {
					results.remove(patient);
				}
			}
		}
		return results;
	}

}
