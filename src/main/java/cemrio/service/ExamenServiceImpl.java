package cemrio.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cemrio.entity.Examen;
import cemrio.entity.User;
import cemrio.exception.ExamenNotFoundException;
import cemrio.repository.IExamenRepo;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ExamenServiceImpl implements IExamenService {
	@Autowired
	private IExamenRepo examenRepo;

	@Autowired
	private IcategorieService categorieService;

	@Autowired
	private AuthorizationService authorizationService;

	@Override
	public Page<Examen> getAll() {
		return examenRepo.findAll(PageRequest.of(0, 30, Sort.by(Sort.Direction.ASC, "nom")));
	}

	@Override
	public Examen get(Long id) {
		return examenRepo.findById(id).orElseThrow(() -> new ExamenNotFoundException(id));
	}

	@Override
	@Transactional
	public Examen add(Examen e) {
		User user = authorizationService.getUser();
		e.setUser(user);
		Examen examen = examenRepo.save(e);
		log.info("Examen " + e.getNom() + " crée" + " par " + user.getUsername());
		return examen;
	}

	@Override
	public List<Examen> getByNom(String nom) {
		List<Examen> examens = examenRepo.findByNomIgnoreCase(nom);
		if (examens.isEmpty()) {
			throw new ExamenNotFoundException(nom);
		}
		return examens;
	}

	@Override
	@Transactional
	public void delete(Long id) {
		log.info("Examen de id " + id + " supprimé par l'utilisateur " + authorizationService.getUser());
		examenRepo.deleteById(id);

	}

	@Override
	@Transactional
	public Examen editName(Long id, @Valid String nom) {
		Examen examen = get(id);
		examen.setNom(nom);
		User user = authorizationService.getUser();
		examen.setUser(user);
		return examenRepo.save(examen);
	}

	@Override
	@Transactional
	public Examen editDescription(Long id, @Valid String description) {
		Examen examen = get(id);
		examen.setDescription(description);
		User user = authorizationService.getUser();
		examen.setUser(user);
		return examenRepo.save(examen);
	}

	@Override
	@Transactional
	public Examen editCategory(Long id, Long categorieId) {
		Examen examen = get(id);
		examen.setCategorie(categorieService.get(categorieId));
		User user = authorizationService.getUser();
		examen.setUser(user);
		return examenRepo.save(examen);
	}

	@Override
	@Transactional
	public Examen edit(Long id, Examen exam) {
		Examen examen = get(id);
		examen.setNom(exam.getNom());
		examen.setDescription(exam.getDescription());
		examen.setCategorie(exam.getCategorie());
		User user = authorizationService.getUser();
		examen.setUser(user);
		return examenRepo.save(examen);
	}

}
