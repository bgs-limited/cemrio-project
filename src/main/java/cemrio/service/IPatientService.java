
package cemrio.service;

import java.time.LocalDate;
import java.util.List;

import javax.validation.Valid;

import org.springframework.data.domain.Page;

import cemrio.entity.Patient;

public interface IPatientService {
	Patient add(Patient patient);

	Patient get(Long id);

	Page<Patient> getAll();

	List<Patient> getByNom(String nom);

	void delete(Long id);

	Patient editName(Long id, String name);

	Patient editSurname(Long id, @Valid String prenom);

	Patient editProfession(Long id, @Valid String profession);

	Patient editAge(Long id, @Valid double age);

	Patient editSex(Long id, @Valid char sexe);

	Patient editTelephone(Long id, @Valid String telephone);

	Patient editDdr(Long id, @Valid LocalDate ddr);

	Patient edit(Long id, Patient patient);

}
