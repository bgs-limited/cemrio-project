package cemrio.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cemrio.entity.Prescripteur;
import cemrio.entity.User;
import cemrio.exception.PrescripteurNotFoundException;
import cemrio.repository.IPrescripteurRepo;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PrescripteurServiceImpl implements IPrescripteurService {

	@Autowired
	private IPrescripteurRepo prescripteurRepo;

	@Autowired
	private AuthorizationService authorizationService;

	@Override
	@Transactional
	public ResponseEntity<?> add(Prescripteur p) {
		if (p.getEmail() != null || p.getTelephone() != null
				&& prescripteurRepo.existsByEmailOrTelephone(p.getEmail(), p.getTelephone())) {
			return ResponseEntity.badRequest().body("Erreur: Ce Prescripteur existe déjà");
		}
		User user = authorizationService.getUser();
		p.setUser(user);
		Prescripteur prescripteur = prescripteurRepo.save(p);
		log.info("Prescripteur " + p.getNom() + " crée" + " par " + user.getUsername());
		return ResponseEntity.ok(prescripteur);
	}

	@Override
	public Prescripteur get(Long id) {
		return prescripteurRepo.findById(id).orElseThrow(() -> new PrescripteurNotFoundException(id));
	}

	@Override
	@Transactional
	public Prescripteur edit(Long id, Prescripteur p) {
		Prescripteur prescripteur = get(id);
		prescripteur.setNom(p.getNom());
		prescripteur.setPrenom(p.getPrenom());
		prescripteur.setEmail(p.getEmail());
		prescripteur.setTelephone(p.getTelephone());
		User user = authorizationService.getUser();
		p.setUser(user);
		return prescripteurRepo.save(prescripteur);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		log.info("prescripteur de id " + id + " supprimé par l'utilisateur " + authorizationService.getUser());
		prescripteurRepo.deleteById(id);
	}

	@Override
	public Page<Prescripteur> getAll() {
		return prescripteurRepo.findAll(PageRequest.of(0, 30, Sort.by(Sort.Direction.ASC, "nom")));
	}

	@Override
	public List<Prescripteur> getByNom(@Valid String nom) {
		List<Prescripteur> prescripteurs = prescripteurRepo.findByNomIgnoreCase(nom);
		if (prescripteurs.isEmpty()) {
			throw new PrescripteurNotFoundException(nom);
		}
		return prescripteurs;
	}

	@Override
	@Transactional
	public Prescripteur editName(Long id, @Valid String nom) {
		Prescripteur prescripteur = get(id);
		prescripteur.setNom(nom);
		User user = authorizationService.getUser();
		prescripteur.setUser(user);
		return prescripteurRepo.save(prescripteur);
	}

	@Override
	public Prescripteur editSurname(Long id, @Valid String prenom) {
		Prescripteur prescripteur = get(id);
		prescripteur.setPrenom(prenom);
		User user = authorizationService.getUser();
		prescripteur.setUser(user);
		return prescripteurRepo.save(prescripteur);
	}

	@Override
	public Prescripteur editTelephone(Long id, @Valid String telephone) {
		Prescripteur prescripteur = get(id);
		prescripteur.setTelephone(telephone);
		User user = authorizationService.getUser();
		prescripteur.setUser(user);
		return prescripteurRepo.save(prescripteur);
	}

	@Override
	public Prescripteur editEmail(Long id, @Valid String email) {
		Prescripteur prescripteur = get(id);
		prescripteur.setEmail(email);
		User user = authorizationService.getUser();
		prescripteur.setUser(user);
		return prescripteurRepo.save(prescripteur);
	}

}
