
package cemrio.service;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.MailException;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cemrio.dto.MessageResponse;
import cemrio.dto.user.UserEditPasswordDto;
import cemrio.entity.ERole;
import cemrio.entity.Role;
import cemrio.entity.User;
import cemrio.exception.UserNotFoundException;
import cemrio.repository.IRoleRepo;
import cemrio.repository.IUserRepo;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@AllArgsConstructor
@Service
public class UserServiceImpl implements IUserService {

	@Autowired
	private IUserRepo userRepo;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	IRoleRepo roleRepo;

	@Autowired
	private MailService mailService;

	@Autowired
	private IServiceService serviceService;

	@Autowired
	private AuthorizationService authorizationService;

	@Override
	@Transactional
	public ResponseEntity<?> add(User u) {
		if (userRepo.existsByUsername(u.getUsername())) {
			log.error("Erreur: Ce nom d'utilisateur est déjà utilisé");
			return ResponseEntity.badRequest()
					.body(new MessageResponse("Erreur: Ce nom d'utilisateur est déjà utilisé"));
		}
		if (u.getEmail() != null && userRepo.existsByEmail(u.getEmail())) {
			log.error("Erreur: Cet email est déjà utilisé");
			return ResponseEntity.badRequest().body(new MessageResponse("Erreur: Cet email est déjà utilisé"));
		}
		User user = new User(u.getUsername(), u.getEmail(), u.getServices(), encoder.encode(u.getPassword()));
		Set<Role> rolesUser = u.getRoles();
		Set<Role> roles = new HashSet<>();
		user.setRoles(rolesUser);
		user.setUserEdit(authorizationService.getUser());
		if (rolesUser == null) {
			Role userRole = roleRepo.findByNom(ERole.ROLE_CLASSE1)
					.orElseThrow(() -> new RuntimeException("Erreur: Role non trouvé."));
			roles.add(userRole);
			user.setRoles(roles);
		}
		User user2 = userRepo.save(user);
		log.info("User " + user2.getUsername() + " ajouté");
		if (user2.getEmail() != null) {
			try {
				mailService.sendEmail(user2);
			} catch (MailException mailException) {
				log.error(mailException.toString());
			}
		}
		return ResponseEntity.ok(user2);
	}

	@Override
	public User get(Long id) {
		return userRepo.findById(id).orElseThrow(() -> new UserNotFoundException(id));
	}

	@Override
	public Page<User> getAll() {
		return userRepo.findAll(PageRequest.of(0, 30, Sort.by(Sort.Direction.ASC, "username")));
	}

	@Override
	@Transactional
	public void delete(Long id) {
		userRepo.deleteById(id);
		log.info("utilisateur de id " + id + " supprimé par l'utilisateur "
				+ authorizationService.getUser().getUsername());
	}

	@Override
	@Transactional
	public User editEmail(Long id, String email) {
		User user = get(id);
		user.setEmail(email);
		User currentUser = authorizationService.getUser();
		user.setUserEdit(currentUser);
		return userRepo.save(user);
	}

	@Override
	@Transactional
	public User editByAdmin(Long id, User u) {
		User user = get(id);
		user.setUsername(u.getUsername());
		user.setEmail(u.getEmail());
		Set<Role> roles = new HashSet<>();
		for (Role role : u.getRoles()) {
			roles.add(role);
		}
		user.setRoles(roles);
		User currentUser = authorizationService.getUser();
		user.setUserEdit(currentUser);
		return userRepo.save(user);
	}

	@Override
	@Transactional
	public void resetPassword(Long id) {
		User user = get(id);
		user.setPassword(encoder.encode("000000"));
		userRepo.save(user);
	}

	@Override
	@Transactional
	public ResponseEntity<String> editPassword(Long id, UserEditPasswordDto userEditPasswordDto) {
		User user = get(id);
		if (!BCrypt.checkpw(userEditPasswordDto.getOldPassword(), user.getPassword())) {
			return ResponseEntity.badRequest().body("Ancien mot de passe incorrect");
		}
		user.setPassword(encoder.encode(userEditPasswordDto.getPassword()));
		userRepo.save(user);
		log.info("password modifié par " + authorizationService.getUser().getUsername());
		return ResponseEntity.ok("Mot de passe changé avec succès");
	}

	@Override
	@Transactional
	public User addService(Long id, Long serviceId) {
		User user = get(id);
		Set<cemrio.entity.Service> services = user.getServices();
		services.add(serviceService.get(serviceId));
		user.setServices(services);
		User currentUser = authorizationService.getUser();
		user.setUserEdit(currentUser);
		return userRepo.save(user);
	}

	@Override
	@Transactional
	public ResponseEntity<?> editService(Long id, Long oldServiceId, Long newServiceId) {
		User user = get(id);
		Set<cemrio.entity.Service> services = user.getServices();
		if (services.isEmpty()) {
			addService(id, newServiceId);
		} else {
			cemrio.entity.Service oldService = serviceService.get(oldServiceId);
			cemrio.entity.Service newService = serviceService.get(newServiceId);
			if (services.contains(oldService)) {
				services.remove(oldService);
				services.add(newService);
			} else {
				return ResponseEntity.badRequest()
						.body(new MessageResponse(
								"Le service que vous souhaité modifié n'est pas dans la liste des services auxquelles appartient l'utilisateur"));
			}

		}
		user.setServices(services);
		User currentUser = authorizationService.getUser();
		user.setUserEdit(currentUser);
		User user2 = userRepo.save(user);
		return ResponseEntity.ok(user2);
	}

	@Override
	@Transactional
	public User editRole(Long id, Long roleId) {
		User user = get(id);
		Set<Role> roles = new HashSet<>();
		Role role = roleRepo.findById(roleId).orElseThrow(() -> new RuntimeException("Erreur: Role non trouvé."));
		roles.add(role);
		user.setRoles(roles);
		User currentUser = authorizationService.getUser();
		user.setUserEdit(currentUser);
		return userRepo.save(user);
	}

	@Override
	public User editUsername(Long id, String username) {
		User user = get(id);
		user.setUsername(username);
		User currentUser = authorizationService.getUser();
		user.setUserEdit(currentUser);
		return userRepo.save(user);
	}

}
