package cemrio.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;

import cemrio.entity.User;
import cemrio.exception.UserNotFoundException;
import cemrio.repository.IRoleRepo;
import cemrio.repository.IUserRepo;
import lombok.extern.slf4j.Slf4j;

@ExtendWith(MockitoExtension.class)
@ContextConfiguration(classes = { UserServiceImpl.class, IUserService.class, IUserRepo.class })
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
@TestPropertySource(locations = "classpath:test.properties")
@Slf4j
class UserServiceTest {

	private IUserService userService;

	@Mock
	private IUserRepo userRepo;

	@Mock
	IRoleRepo roleRepo;

	@BeforeEach
	public void init() {
		userService = new UserServiceImpl(userRepo, null, null, null, null, null);
	}

	@Test
	void getUserById() {
		when(userRepo.findById(1L)).thenReturn(Optional.of(new User()));
		// verify(userRepo, times(1)).findById(1L);
		assertThat(userService.get(1L)).isNotNull().isInstanceOf(User.class);
	}

	@Test
	void getUserByIdNotFound() {
		when(userRepo.findById(99L)).thenThrow(new UserNotFoundException(99L));
		// verify(userRepo).findById(1L);
		assertThatThrownBy(() -> userService.get(99L))
				.isInstanceOf(UserNotFoundException.class)
				.hasMessageContaining("n'a pas été trouvé");
	}

}
