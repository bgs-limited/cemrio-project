package cemrio.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;

import cemrio.entity.Categorie;

@DataJpaTest

@ActiveProfiles(profiles = { "test" })
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
@TestPropertySource(locations = "classpath:test.properties", properties = {
		"spring.liquibase.change-log = classpath:/db/changelog/db.changelog-master.xml" })
public class CategorieTest {

	@Autowired
	private ICategoryRepo categoryRepo;

	@Autowired
	private TestEntityManager entityManager;

	@Test
	void injectedComponentsAreNotNull() {
		assertThat(categoryRepo).isNotNull();
		assertThat(entityManager).isNotNull();
	}

	@Test
	public void getCategoriebyId() {
		Optional<Categorie> categorie = categoryRepo.findById(3L);
		assertThat(categorie).isNotNull().isNotEmpty().isPresent();
	}

	@Test
	public void getCategoriebyIdNotFound() {
		Optional<Categorie> categorie = categoryRepo.findById(11L);
		assertThat(categorie).isNotPresent();
	}

	@Test
	void getAllCategories() {
		final List<Categorie> categories = categoryRepo.findAll();
		assertThat(categories).hasSize(10);
	}

}